from django.conf import settings 
from django.conf.urls.static import static 
from django.views.static import serve 

from django.contrib import admin 
from django.urls import path, include


from .views import get_censos , save_censos , get_especific


urlpatterns = [
	#LA API
	path('loginAPI/', include('rest_auth.urls')),
	path('loginAPI/registration', include('rest_auth.registration.urls')),

	path('get_especific', get_especific.as_view(), name="get_especific"),
	path('get_censos' , get_censos.as_view(), name="get_censos" ),
	path('save_censos', save_censos.as_view(), name="save_censos"),


]