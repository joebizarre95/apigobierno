from .models import censo
from rest_framework.response import Response




def get_all_censos(request, serializer, model):
	obj = model.objects.all().order_by('-id')
	res = serializer(obj , many=True)
	return Response(res.data)




def save_censos( serializer,  request):
	obj = serializer(data=request.data)
	if obj.is_valid():
		obj.save()
		return Response(serializer.data)
	return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)