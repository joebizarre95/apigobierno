from django.contrib.auth import get_user_model
from rest_framework import serializers 
from django.contrib.sites.models import Site
# Get the UserModel
UserModel = get_user_model()
from .models import censo 

class CensosSerializer(serializers.ModelSerializer):
    class Meta:
        model = censo
        fields = ('pk', 'fuente','zona_devolver','zona','sub_zona','stc','atf','razon_Social','canal',
        	'nombre_finca','productor','vereda','municipio','departamento','fase','inventario_anterior',
        	'inventario_actual','diferencia','consumo_concentrado','ventas_contregal','ventas_finca','ventas_bios','marca_concentrado',
        	'categoria','principal_producto','precio_neto_finca','consumo_estimado_sales','marca_sales','principal_producto_b',
        	'precio_neto_finca_b','raza','a_quien_vende_leche','precio_neto_venta_por_litro','correo','telefono_contacto',
        	'premezcla','software' , 'fecha')
      

