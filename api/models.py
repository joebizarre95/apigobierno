from django.db import models
from django.contrib.auth.models import User 
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone 
from datetime import datetime  
from django.contrib.auth.models import User



class Fuente(models.Model):
	nombre = models.CharField(max_length=255)

	def __str__(self):
		return self.nombre



class zona_devolver(models.Model):
	nombre = models.CharField(max_length=255)

	def __str__(self):
		return self.nombre 



class zona(models.Model):
	nombre = models.CharField(max_length=255)

	def __str__(self):
		return self.nombre 



class sub_zona(models.Model):
	nombre = models.CharField(max_length=255)

	def __str__(self):
		return self.nombre 



class stc(models.Model):
	nombre = models.CharField(max_length=255)

	def __str__(self):
		return self.nombre 



class atf(models.Model):
	nombre = models.CharField(max_length=255)

	def __str__(self):
		return self.nombre



class canal(models.Model):
	nombre = models.CharField(max_length=255)

	def __str__(self):
		return self.nombre



class vereda(models.Model):
	nombre = models.CharField(max_length=255)

	def __str__(self):
		return self.nombre 



class fase(models.Model):
	nombre = models.CharField(max_length=255)

	def __str__(self):
		return self.nombre



class marca_concentrado(models.Model):
	nombre = models.CharField(max_length=255)

	def __str__(self):
		return self.nombre



class categoria(models.Model):
	nombre = models.CharField(max_length=255)

	def __str__(self):
		return self.nombre



class marca_sales(models.Model):
	nombre = models.CharField(max_length=255)

	def __str__(self):
		return self.nombre




class raza(models.Model):
	nombre = models.CharField(max_length=255)

	def __str__(self):
		return self.nombre



class a_quien_vende_leche(models.Model):
	nombre = models.CharField(max_length=255)

	def __str__(self):
		return self.nombre



class premezcla(models.Model):
	nombre = models.CharField(max_length=255)

	def __str__(self):
		return self.nombre




class software(models.Model):
	nombre = models.CharField(max_length=255)

	def __str__(self):
		return self.nombre




class departamento(models.Model):
	nombre = models.CharField(max_length=255)

	def __str__(self):
		return self.nombre 



class censo(models.Model):
	fuente = models.ForeignKey(Fuente, on_delete=False)
	zona_devolver = models.ForeignKey(zona_devolver, on_delete=False)
	zona = models.ForeignKey(zona , on_delete=False)
	sub_zona = models.ForeignKey(sub_zona, on_delete=False)
	stc = models.ForeignKey(stc, on_delete=False)
	atf = models.ForeignKey(atf, on_delete=False)
	razon_Social = models.CharField(max_length=255, blank=True)
	canal = models.ForeignKey(canal, on_delete=False)
	nombre_finca = models.CharField(max_length=255, blank=True)
	productor = models.CharField(max_length=255, blank=True)
	vereda = models.ForeignKey(vereda, on_delete=False)
	municipio = models.CharField(max_length=255, blank=True)
	departamento = models.ForeignKey(departamento, on_delete=False)
	fase = models.ForeignKey(fase, on_delete=False)
	inventario_anterior = models.CharField(max_length=255, blank=True)
	inventario_actual = models.CharField(max_length=255, blank=True)
	diferencia = models.CharField(max_length=255, blank=True)
	consumo_concentrado = models.CharField(max_length=255, blank=True)
	ventas_contregal = models.CharField(max_length=255, blank=True)
	ventas_finca = models.CharField(max_length=255, blank=True)
	ventas_bios = models.CharField(max_length=255, blank=True)
	marca_concentrado = models.ForeignKey(marca_concentrado, on_delete=False)
	categoria = models.ForeignKey(categoria, on_delete=False)
	principal_producto = models.CharField(max_length=255, blank=True)
	precio_neto_finca = models.CharField(max_length=255, blank=True)
	consumo_estimado_sales = models.CharField(max_length=255, blank=True)
	marca_sales = models.ForeignKey(marca_sales, on_delete=False)
	principal_producto_b = models.CharField(max_length=255, blank=True)
	precio_neto_finca_b = models.CharField(max_length=255, blank=True)
	raza = models.ForeignKey(raza, on_delete=False)
	a_quien_vende_leche = models.ForeignKey(a_quien_vende_leche, on_delete=False )
	precio_neto_venta_por_litro = models.CharField(max_length=255, blank=True)
	correo = models.CharField(max_length=255, blank=True)
	telefono_contacto = models.CharField(max_length=255, blank=True)
	premezcla = models.ForeignKey(premezcla, on_delete=False)
	software = models.ForeignKey(software, on_delete=False , blank=True, null=True)
	latitud = models.CharField(max_length=255, blank=True)
	longitud = models.CharField(max_length=255, blank=True)
	fecha = models.CharField(default=timezone.now(), max_length=255 ,blank=True)


	def __str__(self):
		return '{}'.format(self.fuente, 
				self.zona_devolver, 
				self.zona,
				self.sub_zona, 
				self.stc, 
				self.atf , 
				self.razon_Social, 
				self.canal, 
				self.nombre_finca,
				self.productor, 
				self.vereda,
				self.municipio, 
				self.departamento, 
				self.fase,
				self.inventario_anterior, 
				self.inventario_actual, 
				self.diferencia, 
				self.consumo_concentrado,
				self.ventas_contregal, 
				self.ventas_finca , 
				self.ventas_bios, 
				self.marca_concentrado, 
				self.categoria, 
				self.principal_producto, 
				self.precio_neto_finca, 
				self.consumo_estimado_sales,
				self.marca_sales, 
				self.principal_producto_b, 
				self.precio_neto_finca_b, 
				self.raza,
				self.a_quien_vende_leche, 
				self.precio_neto_venta_por_litro, 
				self.correo, 
				self.telefono_contacto,
				self.premezcla, 
				self.software,
				self.latitud,
				self.longitud,
				self.fecha,  )

