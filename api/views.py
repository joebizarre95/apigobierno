from django.shortcuts import render

from rest_framework import viewsets
from django.core import serializers
import json

from django.shortcuts import render
from django.shortcuts import get_object_or_404 , get_list_or_404
from django.http import HttpResponse , JsonResponse
from django.contrib.auth import authenticate
from django.views.decorators.csrf import csrf_exempt
from urllib.parse import urlencode 
from django.http import QueryDict
#rest framework
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import mixins 
from rest_framework import generics 
from rest_framework import status 


# Create your views here

from .functions import get_all_censos
from .models import censo

from .serializers import CensosSerializer



class get_censos(APIView):
	def get(self, request):
		response = get_all_censos(request, CensosSerializer, censos)
		return response




class save_censos(APIView):
	def post(self, request):
		obj = CensosSerializer(data = request.data)
		if obj.is_valid():
			obj.save()
			return Response(obj.data)
		return Response(obj.errors)



class get_especific(APIView):
	def post(self, request):
		user = request.data['pk']
		obj = censos.objects.filter(pk = user)
		res = CensosSerializer(obj , many=True)
		return Response(res.data)









