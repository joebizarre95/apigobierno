from django.contrib import admin

from .models import (Fuente , zona_devolver ,  zona , sub_zona , stc , atf , canal ,
						 vereda , fase , marca_concentrado , categoria , marca_sales , raza , a_quien_vende_leche ,
						 premezcla, software , departamento , censo)




@admin.register(Fuente)
class FuenteAdmimn(admin.ModelAdmin):
	pass



@admin.register(zona_devolver)
class zona_devolver(admin.ModelAdmin):
	pass 



@admin.register(zona)
class zona(admin.ModelAdmin):
	pass 


@admin.register(sub_zona)
class sub_zona(admin.ModelAdmin):
	pass 



@admin.register(stc)
class stc(admin.ModelAdmin):
	pass 



@admin.register(atf)
class atf(admin.ModelAdmin):
	pass


@admin.register(canal)
class canal(admin.ModelAdmin):
	pass


@admin.register(vereda)
class vereda(admin.ModelAdmin):
	pass 


@admin.register(fase)
class fase(admin.ModelAdmin):
	pass 


@admin.register(marca_concentrado)
class marca_concentrado(admin.ModelAdmin):
	pass 


@admin.register(categoria)
class categoria(admin.ModelAdmin):
	pass



@admin.register(marca_sales)
class marca_sales(admin.ModelAdmin):
	pass



@admin.register(raza)
class raza(admin.ModelAdmin):
	pass



@admin.register(a_quien_vende_leche)
class a_quien_vende_leche(admin.ModelAdmin):
	pass




@admin.register(premezcla)
class premezcla(admin.ModelAdmin):
	pass



@admin.register(software)
class software(admin.ModelAdmin):
	pass




@admin.register(departamento)
class departamento(admin.ModelAdmin):
	pass



@admin.register(censo)
class censo(admin.ModelAdmin):
	pass



