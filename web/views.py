from django.shortcuts import render
from django.views.generic import TemplateView , CreateView, ListView, DetailView, DeleteView, UpdateView
# Create your views here.

from django.contrib.auth import authenticate , login
from django.shortcuts import redirect

from django.contrib.auth.views import LogoutView
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_list_or_404

from api.models import ( Fuente , zona_devolver , zona , sub_zona , stc , atf , canal , vereda , fase , 
	marca_concentrado , categoria , marca_sales , raza ,  a_quien_vende_leche ,  premezcla ,  software ,
	departamento , censo ,)
from django.core import serializers
from django.urls import reverse_lazy

from datetime import datetime 
from django.contrib.auth.models import User
current_moth = datetime.now().month
from django.shortcuts import render_to_response




class index(TemplateView):
	template_name = 'login.html'

	def post(self, request):
		user = request.POST.get('username')
		passw = request.POST.get('password')
		user = authenticate(username = user , password = passw)
		if user is not None:
			login(self.request, user)
			return redirect('home')
		else:
			return redirect('index')





def log_out(LogoutView):
	return redirect('index')


class home(TemplateView):
	template_name = 'home.html'



class mapa(TemplateView):
	template_name = 'mapa.html'

	def get_context_data(self, *args, **kwargs):
		context = super(mapa, self).get_context_data(**kwargs)
		context['data'] = censo.objects.all()
		context['users'] = User.objects.all()
		context['Fuente'] = Fuente.objects.all()
		context['zona_devolver'] = zona_devolver.objects.all()
		context['zona'] = zona.objects.all()
		context['sub_zona'] = sub_zona.objects.all()
		context['stc'] = stc.objects.all()
		context['atf'] = atf.objects.all()
		context['canal'] = canal.objects.all()
		context['vereda'] = vereda.objects.all()
		context['fase'] = fase.objects.all()
		context['marca_concentrado'] = marca_concentrado.objects.all()
		context['categoria'] = categoria.objects.all()
		context['marca_sales'] = marca_sales.objects.all()
		context['raza'] = raza.objects.all()
		context['a_quien_vende_leche'] = a_quien_vende_leche.objects.all()
		context['premezcla'] = premezcla.objects.all()
		context['software'] = software.objects.all()
		#context['json'] = serializers.serialize("json", censo.objects.filter(mes=current_moth))
		return context




class filtrarProductor(DetailView):
	template_name = 'mapa.html'
	model = User

	def get_context_data(self, *args, **kwargs):
		context = super(filtrarProductor, self).get_context_data(**kwargs)
		context['data'] = censo.objects.filter(productor_id = self.kwargs['pk'])
		return context

	

class busqueda_usuario(TemplateView):
	template_name = 'mapa.html'

	def post(self, request):
		item = request.POST.get('data')
		data = censo.objects.filter(productor = item)
		return data




class busqueda_fuente(DetailView):
	template_name = 'mapa.html'
	model = Fuente

	def get_context_data(self, *args, **kwargs):
		context = super(busqueda_fuente, self).get_context_data(**kwargs)
		context['data'] = censo.objects.filter(fuente = self.kwargs['pk'])
		context['users'] = User.objects.all()
		context['Fuente'] = Fuente.objects.all()
		context['zona_devolver'] = zona_devolver.objects.all()
		context['zona'] = zona.objects.all()
		context['sub_zona'] = sub_zona.objects.all()
		context['stc'] = stc.objects.all()
		context['atf'] = atf.objects.all()
		context['canal'] = canal.objects.all()
		context['vereda'] = vereda.objects.all()
		context['fase'] = fase.objects.all()
		context['marca_concentrado'] = marca_concentrado.objects.all()
		context['categoria'] = categoria.objects.all()
		context['marca_sales'] = marca_sales.objects.all()
		context['raza'] = raza.objects.all()
		context['a_quien_vende_leche'] = a_quien_vende_leche.objects.all()
		context['premezcla'] = premezcla.objects.all()
		context['software'] = software.objects.all()
		return context




class busqueda_zona_devolver(DetailView):
	template_name = 'mapa.html'
	model = zona_devolver 

	def get_context_data(self, *args , **kwargs):
		context = super(busqueda_zona_devolver, self).get_context_data(**kwargs)
		context['data'] = censo.objects.filter(zona_devolver = self.kwargs['pk'])
		context['users'] = User.objects.all()
		context['Fuente'] = Fuente.objects.all()
		context['zona_devolver'] = zona_devolver.objects.all()
		context['zona'] = zona.objects.all()
		context['sub_zona'] = sub_zona.objects.all()
		context['stc'] = stc.objects.all()
		context['atf'] = atf.objects.all()
		context['canal'] = canal.objects.all()
		context['vereda'] = vereda.objects.all()
		context['fase'] = fase.objects.all()
		context['marca_concentrado'] = marca_concentrado.objects.all()
		context['categoria'] = categoria.objects.all()
		context['marca_sales'] = marca_sales.objects.all()
		context['raza'] = raza.objects.all()
		context['a_quien_vende_leche'] = a_quien_vende_leche.objects.all()
		context['premezcla'] = premezcla.objects.all()
		context['software'] = software.objects.all()
		return context





class busqueda_sub_zona(DetailView):
	template_name = 'mapa.html'
	model = sub_zona 


	def get_context_data(self, *args , **kwargs):
		context = super(busqueda_sub_zona, self).get_context_data(**kwargs)
		context['data'] = censo.objects.filter(sub_zona = self.kwargs['pk'])
		context['users'] = User.objects.all()
		context['Fuente'] = Fuente.objects.all()
		context['zona_devolver'] = zona_devolver.objects.all()
		context['zona'] = zona.objects.all()
		context['sub_zona'] = sub_zona.objects.all()
		context['stc'] = stc.objects.all()
		context['atf'] = atf.objects.all()
		context['canal'] = canal.objects.all()
		context['vereda'] = vereda.objects.all()
		context['fase'] = fase.objects.all()
		context['marca_concentrado'] = marca_concentrado.objects.all()
		context['categoria'] = categoria.objects.all()
		context['marca_sales'] = marca_sales.objects.all()
		context['raza'] = raza.objects.all()
		context['a_quien_vende_leche'] = a_quien_vende_leche.objects.all()
		context['premezcla'] = premezcla.objects.all()
		context['software'] = software.objects.all()
		return context





class busqueda_zona(DetailView):
	template_name = 'mapa.html'
	model = zona

	def get_context_data(self, *args, **kwargs):
		context = super(busqueda_zona, self).get_context_data(**kwargs)
		context['data'] = censo.objects.filter(zona = self.kwargs['pk'])
		context['users'] = User.objects.all()
		context['Fuente'] = Fuente.objects.all()
		context['zona_devolver'] = zona_devolver.objects.all()
		context['zona'] = zona.objects.all()
		context['sub_zona'] = sub_zona.objects.all()
		context['stc'] = stc.objects.all()
		context['atf'] = atf.objects.all()
		context['canal'] = canal.objects.all()
		context['vereda'] = vereda.objects.all()
		context['fase'] = fase.objects.all()
		context['marca_concentrado'] = marca_concentrado.objects.all()
		context['categoria'] = categoria.objects.all()
		context['marca_sales'] = marca_sales.objects.all()
		context['raza'] = raza.objects.all()
		context['a_quien_vende_leche'] = a_quien_vende_leche.objects.all()
		context['premezcla'] = premezcla.objects.all()
		context['software'] = software.objects.all()
		return context







class busqueda_stc(DetailView):
	template_name = 'mapa.html'
	model = stc 

	def get_context_data(self, *args, **kwargs):
		context = super(busqueda_stc, self).get_context_data(**kwargs)
		context['data'] = censo.objects.filter(stc = self.kwargs['pk'])
		context['users'] = User.objects.all()
		context['Fuente'] = Fuente.objects.all()
		context['zona_devolver'] = zona_devolver.objects.all()
		context['zona'] = zona.objects.all()
		context['sub_zona'] = sub_zona.objects.all()
		context['stc'] = stc.objects.all()
		context['atf'] = atf.objects.all()
		context['canal'] = canal.objects.all()
		context['vereda'] = vereda.objects.all()
		context['fase'] = fase.objects.all()
		context['marca_concentrado'] = marca_concentrado.objects.all()
		context['categoria'] = categoria.objects.all()
		context['marca_sales'] = marca_sales.objects.all()
		context['raza'] = raza.objects.all()
		context['a_quien_vende_leche'] = a_quien_vende_leche.objects.all()
		context['premezcla'] = premezcla.objects.all()
		context['software'] = software.objects.all()
		return context




class busqueda_atf(DetailView):
	template_name = 'mapa.html'
	model = atf 

	def get_context_data(self, *args, **kwargs):
		context = super(busqueda_atf, self).get_context_data(**kwargs)
		context['data'] = censo.objects.filter(atf = self.kwargs['pk'])
		context['users'] = User.objects.all()
		context['Fuente'] = Fuente.objects.all()
		context['zona_devolver'] = zona_devolver.objects.all()
		context['zona'] = zona.objects.all()
		context['sub_zona'] = sub_zona.objects.all()
		context['stc'] = stc.objects.all()
		context['atf'] = atf.objects.all()
		context['canal'] = canal.objects.all()
		context['vereda'] = vereda.objects.all()
		context['fase'] = fase.objects.all()
		context['marca_concentrado'] = marca_concentrado.objects.all()
		context['categoria'] = categoria.objects.all()
		context['marca_sales'] = marca_sales.objects.all()
		context['raza'] = raza.objects.all()
		context['a_quien_vende_leche'] = a_quien_vende_leche.objects.all()
		context['premezcla'] = premezcla.objects.all()
		context['software'] = software.objects.all()
		return context



class busqueda_canal(DetailView):
	template_name = 'mapa.html'
	model = canal 

	def get_context_data(self, *args, **kwargs):
		context = super(busqueda_canal, self).get_context_data(**kwargs)
		context['data'] = censo.objects.filter(canal = self.kwargs['pk'])
		context['users'] = User.objects.all()
		context['Fuente'] = Fuente.objects.all()
		context['zona_devolver'] = zona_devolver.objects.all()
		context['zona'] = zona.objects.all()
		context['sub_zona'] = sub_zona.objects.all()
		context['stc'] = stc.objects.all()
		context['atf'] = atf.objects.all()
		context['canal'] = canal.objects.all()
		context['vereda'] = vereda.objects.all()
		context['fase'] = fase.objects.all()
		context['marca_concentrado'] = marca_concentrado.objects.all()
		context['categoria'] = categoria.objects.all()
		context['marca_sales'] = marca_sales.objects.all()
		context['raza'] = raza.objects.all()
		context['a_quien_vende_leche'] = a_quien_vende_leche.objects.all()
		context['premezcla'] = premezcla.objects.all()
		context['software'] = software.objects.all()
		return context




class busqueda_vereda(DetailView):
	template_name = 'mapa.html'
	model = vereda 

	def get_context_data(self, *args, **kwargs):
		context = super(busqueda_vereda, self).get_context_data(**kwargs)
		context['data'] = censo.objects.filter(vereda = self.kwargs['pk'])
		context['users'] = User.objects.all()
		context['Fuente'] = Fuente.objects.all()
		context['zona_devolver'] = zona_devolver.objects.all()
		context['zona'] = zona.objects.all()
		context['sub_zona'] = sub_zona.objects.all()
		context['stc'] = stc.objects.all()
		context['atf'] = atf.objects.all()
		context['canal'] = canal.objects.all()
		context['vereda'] = vereda.objects.all()
		context['fase'] = fase.objects.all()
		context['marca_concentrado'] = marca_concentrado.objects.all()
		context['categoria'] = categoria.objects.all()
		context['marca_sales'] = marca_sales.objects.all()
		context['raza'] = raza.objects.all()
		context['a_quien_vende_leche'] = a_quien_vende_leche.objects.all()
		context['premezcla'] = premezcla.objects.all()
		context['software'] = software.objects.all()
		return context






class busqueda_fase(DetailView):
	template_name = 'mapa.html'
	model = fase 

	def get_context_data(self, *args, **kwargs):
		context = super(busqueda_fase, self).get_context_data(**kwargs)
		context['data'] = censo.objects.filter(fase = self.kwargs['pk'])
		context['users'] = User.objects.all()
		context['Fuente'] = Fuente.objects.all()
		context['zona_devolver'] = zona_devolver.objects.all()
		context['zona'] = zona.objects.all()
		context['sub_zona'] = sub_zona.objects.all()
		context['stc'] = stc.objects.all()
		context['atf'] = atf.objects.all()
		context['canal'] = canal.objects.all()
		context['vereda'] = vereda.objects.all()
		context['fase'] = fase.objects.all()
		context['marca_concentrado'] = marca_concentrado.objects.all()
		context['categoria'] = categoria.objects.all()
		context['marca_sales'] = marca_sales.objects.all()
		context['raza'] = raza.objects.all()
		context['a_quien_vende_leche'] = a_quien_vende_leche.objects.all()
		context['premezcla'] = premezcla.objects.all()
		context['software'] = software.objects.all()
		return context







class busqueda_marca_concentrado(DetailView):
	template_name = 'mapa.html'
	model = marca_concentrado 

	def get_context_data(self, *args, **kwargs):
		context = super(busqueda_marca_concentrado, self).get_context_data(**kwargs)
		context['data'] = censo.objects.filter(marca_concentrado = self.kwargs['pk'])
		context['users'] = User.objects.all()
		context['Fuente'] = Fuente.objects.all()
		context['zona_devolver'] = zona_devolver.objects.all()
		context['zona'] = zona.objects.all()
		context['sub_zona'] = sub_zona.objects.all()
		context['stc'] = stc.objects.all()
		context['atf'] = atf.objects.all()
		context['canal'] = canal.objects.all()
		context['vereda'] = vereda.objects.all()
		context['fase'] = fase.objects.all()
		context['marca_concentrado'] = marca_concentrado.objects.all()
		context['categoria'] = categoria.objects.all()
		context['marca_sales'] = marca_sales.objects.all()
		context['raza'] = raza.objects.all()
		context['a_quien_vende_leche'] = a_quien_vende_leche.objects.all()
		context['premezcla'] = premezcla.objects.all()
		context['software'] = software.objects.all()
		return context




class busqueda_categoria(DetailView):
	template_name = 'mapa.html'
	model = categoria 

	def get_context_data(self, *args, **kwargs):
		context = super(busqueda_categoria, self).get_context_data(**kwargs)
		context['data'] = censo.objects.filter(categoria = self.kwargs['pk'])
		context['users'] = User.objects.all()
		context['Fuente'] = Fuente.objects.all()
		context['zona_devolver'] = zona_devolver.objects.all()
		context['zona'] = zona.objects.all()
		context['sub_zona'] = sub_zona.objects.all()
		context['stc'] = stc.objects.all()
		context['atf'] = atf.objects.all()
		context['canal'] = canal.objects.all()
		context['vereda'] = vereda.objects.all()
		context['fase'] = fase.objects.all()
		context['marca_concentrado'] = marca_concentrado.objects.all()
		context['categoria'] = categoria.objects.all()
		context['marca_sales'] = marca_sales.objects.all()
		context['raza'] = raza.objects.all()
		context['a_quien_vende_leche'] = a_quien_vende_leche.objects.all()
		context['premezcla'] = premezcla.objects.all()
		context['software'] = software.objects.all()
		return context






class busqueda_marca_sales(DetailView):
	template_name = 'mapa.html'
	model = marca_sales 

	def get_context_data(self, *args, **kwargs):
		context = super(busqueda_marca_sales, self).get_context_data(**kwargs)
		context['data'] = censo.objects.filter(marca_sales = self.kwargs['pk'])
		context['users'] = User.objects.all()
		context['Fuente'] = Fuente.objects.all()
		context['zona_devolver'] = zona_devolver.objects.all()
		context['zona'] = zona.objects.all()
		context['sub_zona'] = sub_zona.objects.all()
		context['stc'] = stc.objects.all()
		context['atf'] = atf.objects.all()
		context['canal'] = canal.objects.all()
		context['vereda'] = vereda.objects.all()
		context['fase'] = fase.objects.all()
		context['marca_concentrado'] = marca_concentrado.objects.all()
		context['categoria'] = categoria.objects.all()
		context['marca_sales'] = marca_sales.objects.all()
		context['raza'] = raza.objects.all()
		context['a_quien_vende_leche'] = a_quien_vende_leche.objects.all()
		context['premezcla'] = premezcla.objects.all()
		context['software'] = software.objects.all()
		return context





class busqueda_raza(DetailView):
	template_name = 'mapa.html'
	model = raza 

	def get_context_data(self, *args, **kwargs):
		context = super(busqueda_raza, self).get_context_data(**kwargs)
		context['data'] = censo.objects.filter(raza = self.kwargs['pk'])
		context['users'] = User.objects.all()
		context['Fuente'] = Fuente.objects.all()
		context['zona_devolver'] = zona_devolver.objects.all()
		context['zona'] = zona.objects.all()
		context['sub_zona'] = sub_zona.objects.all()
		context['stc'] = stc.objects.all()
		context['atf'] = atf.objects.all()
		context['canal'] = canal.objects.all()
		context['vereda'] = vereda.objects.all()
		context['fase'] = fase.objects.all()
		context['marca_concentrado'] = marca_concentrado.objects.all()
		context['categoria'] = categoria.objects.all()
		context['marca_sales'] = marca_sales.objects.all()
		context['raza'] = raza.objects.all()
		context['a_quien_vende_leche'] = a_quien_vende_leche.objects.all()
		context['premezcla'] = premezcla.objects.all()
		context['software'] = software.objects.all()
		return context




class busqueda_a_quien_vende_leche(DetailView):
	template_name = 'mapa.html'
	model = a_quien_vende_leche 

	def get_context_data(self, *args, **kwargs):
		context = super(busqueda_a_quien_vende_leche, self).get_context_data(**kwargs)
		context['data'] = censo.objects.filter(a_quien_vende_leche = self.kwargs['pk'])
		context['users'] = User.objects.all()
		context['Fuente'] = Fuente.objects.all()
		context['zona_devolver'] = zona_devolver.objects.all()
		context['zona'] = zona.objects.all()
		context['sub_zona'] = sub_zona.objects.all()
		context['stc'] = stc.objects.all()
		context['atf'] = atf.objects.all()
		context['canal'] = canal.objects.all()
		context['vereda'] = vereda.objects.all()
		context['fase'] = fase.objects.all()
		context['marca_concentrado'] = marca_concentrado.objects.all()
		context['categoria'] = categoria.objects.all()
		context['marca_sales'] = marca_sales.objects.all()
		context['raza'] = raza.objects.all()
		context['a_quien_vende_leche'] = a_quien_vende_leche.objects.all()
		context['premezcla'] = premezcla.objects.all()
		context['software'] = software.objects.all()
		return context




class busqueda_premezcla(DetailView):
	template_name = 'mapa.html'
	model = premezcla 

	def get_context_data(self, *args, **kwargs):
		context = super(busqueda_premezcla, self).get_context_data(**kwargs)
		context['data'] = censo.objects.filter(premezcla = self.kwargs['pk'])
		context['users'] = User.objects.all()
		context['Fuente'] = Fuente.objects.all()
		context['zona_devolver'] = zona_devolver.objects.all()
		context['zona'] = zona.objects.all()
		context['sub_zona'] = sub_zona.objects.all()
		context['stc'] = stc.objects.all()
		context['atf'] = atf.objects.all()
		context['canal'] = canal.objects.all()
		context['vereda'] = vereda.objects.all()
		context['fase'] = fase.objects.all()
		context['marca_concentrado'] = marca_concentrado.objects.all()
		context['categoria'] = categoria.objects.all()
		context['marca_sales'] = marca_sales.objects.all()
		context['raza'] = raza.objects.all()
		context['a_quien_vende_leche'] = a_quien_vende_leche.objects.all()
		context['premezcla'] = premezcla.objects.all()
		context['software'] = software.objects.all()
		return context





class busqueda_software(DetailView):
	template_name = 'mapa.html'
	model = software 

	def get_context_data(self, *args, **kwargs):
		context = super(busqueda_software, self).get_context_data(**kwargs)
		context['data'] = censo.objects.filter(software = self.kwargs['pk'])
		context['users'] = User.objects.all()
		context['Fuente'] = Fuente.objects.all()
		context['zona_devolver'] = zona_devolver.objects.all()
		context['zona'] = zona.objects.all()
		context['sub_zona'] = sub_zona.objects.all()
		context['stc'] = stc.objects.all()
		context['atf'] = atf.objects.all()
		context['canal'] = canal.objects.all()
		context['vereda'] = vereda.objects.all()
		context['fase'] = fase.objects.all()
		context['marca_concentrado'] = marca_concentrado.objects.all()
		context['categoria'] = categoria.objects.all()
		context['marca_sales'] = marca_sales.objects.all()
		context['raza'] = raza.objects.all()
		context['a_quien_vende_leche'] = a_quien_vende_leche.objects.all()
		context['premezcla'] = premezcla.objects.all()
		context['software'] = software.objects.all()
		return context





class busqueda_previo(TemplateView):
	template_name = 'mapa.html'

	def post(self, request):
		previo = request.POST.get('previo')
		if previo:
			results = censo.objects.filter(nombre_finca = previo)
		else:
			results = []
		return render_to_response('mapa2.html' , {"data" : results })





class busqueda_productor(TemplateView):
	template_name = 'mapa.html'

	def post(self, request):
		previo = request.POST.get('previo')
		if previo:
			results = censo.objects.filter(productor = previo)
		else:
			results = []
		return render_to_response('mapa2.html' , {"data" : results })





class Filtros(DetailView):
	template_name = 'mapa.html'
	model = censo

	def get_context_data(self, *args , **kwargs):
		context = super(Filtros, self).get_context_data(**kwargs)
		context['data'] = censo.objects.filter(fuente = self.kwargs['pk']) 
		if(context):
			return context 
		else:
			context['data'] = censo.objects.filter(zona_devolver = self.kwargs['pk'])
			if(context):
				return context
			else:
				context['data'] = censo.objects.filter(zona = self.kwargs['pk'])
				if(conttext):
					return context 
				else: 
					context['data'] = censo.objects.filter(sub_zona = self.kwargs['pk'])
					if(context):
						return context 
					else:
						context['data'] = censo.objects.filter(stc = self.kwargs['pk'])
						if(context):
							return context 
						else:
							context['data'] = censo.objects.filter(atf = self.kwargs['pk'])
							if(context):
								return context
							else:
								context['data'] = censo.objects.filter(canal = self.kwargs['pk'])
								if(context):
									return context
								else:
									context['data'] = censo.objects.filter(vereda = self.kwargs['pk'])
									if(context):
										return context
									else:
										context['data'] = censo.objects.filter(departamento = self.kwargs['pk'])
										if(context):
											return context
										else:
											context['data'] = censo.objects.filter(fase = self.kwargs['pk'])
											if(context):
												return context
											else:
												context['data'] = censo.objects.filter(marca_concentrado = self.kwargs['pk'])
												if(context):
													return context
												else:
													context['data'] = censo.objects.filter(categoria = self.kwargs['pk'])
													if(context):
														return context
													else:
														context['data'] = censo.objects.filter(marca_sales = self.kwargs['pk'])
														if(context):
															return context
														else:
															context['data'] = censo.objects.filter(raza = self.kwargs['pk'])
															if(context):
																return context
															else:
																context['data'] = censo.objects.filter(a_quien_vende_leche = self.kwargs['pk'])
																if(context):
																	return context
																else:
																	context['data'] = censo.objects.filter(software = self.kwargs['pk'])
																	if(context):
																		return context








class detalles(DetailView):
	template_name = 'detalles_mapa.html'
	model = censo

	def get_context_data(self, *args, **kwargs):
		context = super(detalles, self).get_context_data(**kwargs)
		context['data'] = censo.objects.all()
		context['user'] = User.objects.all()
		return context




#createviews 
class Canales(CreateView):
	template_name = 'listar_c.html'
	model = canal 
	fields = '__all__'
	success_url = reverse_lazy('canales')


	def get_context_data(self , *args , **kwargs):
		context = super(Canales, self).get_context_data(**kwargs)
		context['data'] = canal.objects.order_by('-id')
		return context




class EliminarCanal(DeleteView):
	model = canal 
	template_name = 'eliminar_c.html'
	success_url = reverse_lazy('canales')


	def get_context_data(self, *args, **kwargs):
		context = super(EliminarCanal, self).get_context_data(**kwargs)
		context['data'] = canal.objects.order_by('-id')
		return context 







#createviews
class Categorias(CreateView):
	model = categoria
	template_name = 'listar_cat.html'
	fields = '__all__'
	success_url = reverse_lazy('categorias')

	def get_context_data(self, *args, **kwargs):
		context = super(Categorias, self).get_context_data(**kwargs)
		context['data'] = categoria.objects.order_by('-id')
		return context 


class EliminarCategoria(DeleteView):
	model = categoria
	template_name = 'eliminar_cat.html'
	success_url = reverse_lazy('categorias')

	def get_context_data(self, *args, **kwargs):
		context = super(EliminarCategoria, self).get_context_data(**kwargs)
		context['data'] = categoria.objects.order_by('-id')
		return context  




class Estadisticas(TemplateView):
	template_name = 'estadisticas.html'


	def get_context_data(self, *args, **kwargs):
		context = super(Estadisticas, self).get_context_data(**kwargs)
		context['data'] = censo.objects.all().count()
		context['quien1'] = censo.objects.filter(a_quien_vende_leche=1).count()
		context['quien2'] = censo.objects.filter(a_quien_vende_leche=2).count()
		context['quien3'] = censo.objects.filter(a_quien_vende_leche=3).count()
		context['quien4'] = censo.objects.filter(a_quien_vende_leche=4).count()
		context['quien5'] = censo.objects.filter(a_quien_vende_leche=6).count()
		context['stc1'] = censo.objects.filter(stc=1).count()
		context['stc2'] = censo.objects.filter(stc=2).count()
		context['stc3'] = censo.objects.filter(stc=3).count()
		context['atf1'] = censo.objects.filter(atf=1).count()
		context['atf2'] = censo.objects.filter(atf=2).count()
		context['atf3'] = censo.objects.filter(atf=3).count()
		context['concentrado1'] = censo.objects.filter(marca_concentrado=1).count()
		context['concentrado2'] = censo.objects.filter(marca_concentrado=2).count()
		context['concentrado3'] = censo.objects.filter(marca_concentrado=3).count()
		context['concentrado4'] = censo.objects.filter(marca_concentrado=4).count()
		context['sales1'] = censo.objects.filter(marca_sales=1).count()
		context['sales2'] = censo.objects.filter(marca_sales=2).count()
		context['sales3'] = censo.objects.filter(marca_sales=3).count()
		context['sales4'] = censo.objects.filter(marca_sales=4).count()
		context['sales5'] = censo.objects.filter(marca_sales=5).count()
		context['sales6'] = censo.objects.filter(marca_sales=6).count()
		context['raza1'] = censo.objects.filter(raza=1).count()
		context['raza2'] = censo.objects.filter(raza=2).count()
		context['raza3'] = censo.objects.filter(raza=3).count()
		context['raza4'] = censo.objects.filter(raza=4).count()
		context['vereda1'] = censo.objects.filter(vereda=1).count()
		context['vereda2'] = censo.objects.filter(vereda=2).count()
		context['vereda3'] = censo.objects.filter(vereda=3).count()
		context['vereda4'] = censo.objects.filter(vereda=4).count()
		context['vereda5'] = censo.objects.filter(vereda=5).count()
		context['vereda6'] = censo.objects.filter(vereda=6).count()
		context['fase1'] = censo.objects.filter(fase=1).count()
		context['fase2'] = censo.objects.filter(fase=2).count()
		return context



