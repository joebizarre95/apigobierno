from django.conf import settings 
from django.conf.urls.static import static 
from django.views.static import serve 

from django.contrib import admin 
from django.urls import path, include
from django.views.decorators.csrf import csrf_exempt

from .views import ( index ,home , log_out,  mapa , Canales , EliminarCanal  , Categorias, EliminarCategoria ,
									 detalles , Estadisticas ,filtrarProductor , Filtros, busqueda_usuario , busqueda_fuente, 
					busqueda_zona_devolver , busqueda_zona , busqueda_sub_zona , busqueda_stc , busqueda_atf , busqueda_canal ,
						busqueda_vereda ,  busqueda_fase , busqueda_marca_concentrado , busqueda_categoria , busqueda_marca_sales ,
						busqueda_raza, busqueda_a_quien_vende_leche , busqueda_premezcla , busqueda_software , busqueda_previo , 
						busqueda_productor)  


urlpatterns = [
	#LA API

	path('index/', index.as_view(), name="index"),
	path('log_out', log_out, name="log_out"),
	path('home/', home.as_view(), name="home"),
	path('mapa/', mapa.as_view(), name="mapa"),
	path('estadisticas/' , Estadisticas.as_view(), name="estadisticas"),
	path('filtrarProductor/<int:pk>' , filtrarProductor.as_view(), name="filtrarProductor"),
	path('filtros/<int:pk>', Filtros.as_view(), name="Filtros"),


	#createviews
	path('canales/' , Canales.as_view() , name="canales"),
	path('eliminar_c/<int:pk>', EliminarCanal.as_view(), name="eliminar_c"),


	path('categorias', Categorias.as_view(), name="categorias"),
	path('eliminar_cat/<int:pk>', EliminarCategoria.as_view(), name="eliminar_cat"),

	
	path('detalles/<int:pk>', detalles.as_view(), name="detalles"),



	path('busqueda_usuario' , busqueda_usuario.as_view(), name="busqueda_usuario"),
	path('busqueda_fuente/<int:pk>' , busqueda_fuente.as_view() , name="busqueda_fuente"),
	path('busqueda_zona_devolver/<int:pk>' , busqueda_zona_devolver.as_view(), name="busqueda_zona_devolver"),
	path('busqueda_zona/<int:pk>', busqueda_zona.as_view(), name="busqueda_zona"),
	path('busqueda_sub_zona/<int:pk>' , busqueda_sub_zona.as_view(), name="busqueda_sub_zona"),
	path('busqueda_stc/<int:pk>' ,  busqueda_stc.as_view(), name="busqueda_stc"),
	path('busqueda_atf/<int:pk>', busqueda_atf.as_view(), name="busqueda_atf"),
	path('busqueda_canal/<int:pk>', busqueda_canal.as_view(), name="busqueda_canal"),
	path('busqueda_vereda/<int:pk>', busqueda_vereda.as_view() , name="busqueda_vereda"),
	path('busqueda_fase/<int:pk>' , busqueda_fase.as_view(), name="busqueda_fase"), 
	path('busqueda_marca_concentrado/<int:pk>', busqueda_marca_concentrado.as_view(), name="busqueda_marca_concentrado"),
	path('busqueda_categoria/<int:pk>', busqueda_categoria.as_view(), name="busqueda_categoria"),
	path('busqueda_marca_sales/<int:pk>', busqueda_marca_sales.as_view(), name="busqueda_marca_sales"),
	path('busqueda_raza/<int:pk>', busqueda_raza.as_view(), name="busqueda_raza"),
	path('busqueda_a_quien_vende_leche/<int:pk>', busqueda_a_quien_vende_leche.as_view(), name="busqueda_a_quien_vende_leche"),
	path('busqueda_premezcla/<int:pk>', busqueda_premezcla.as_view(), name="busqueda_premezcla"),
	path('busqueda_software/<int:pk>', busqueda_software.as_view(), name="busqueda_software"),

	path('busqueda_previo', csrf_exempt(busqueda_previo.as_view()), name="busqueda_previo"),
	path('busqueda_productor', csrf_exempt(busqueda_productor.as_view()), name="busqueda_productor"),
	
	










]